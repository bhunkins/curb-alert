/*******************************************************************************
 * CURB ALERT -- SENSOR UNIT
 * 
 * This sketch listens for a ping from the head unit, and will transmit it's
 * data back to the head unit. This unit uses an ultrasonic sensor to detect
 * the distance to the curb.
 *  
********************************************************************************/

/** SONAR PINOUTS *********

    VCC   --> 5V
    GND   --> GND
    ECHO  --> D5
    TRIG  --> D6

 /** RADIO PINOUTS *******

    1 GND   --> GND
    2 VCC   --> 3.3/5V
    3 CE    --> D9
    4 CSN   --> D10
    5 SCK   --> D13
    6 MOSI  --> D11
    7 MISO  --> D12
    8 IRQ   --> NOT USED
 
 ************************/

/* nRF24L01 libs */
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/* sonar sensor lib */
#include <NewPing.h>

/* radio setup */
RF24 radio(9, 10);
const uint64_t pipes[3] = { 0xE8E8F0F0E1LL, 0xE8E8F0F0E2LL, 0xE8E8F0F0E3LL }; // head unit, front sensor, rear sensor

/* sensor setup */
#define TRIGGER_PIN   6 // Arduino pin tied to trigger pin on ping sensor.
#define ECHO_PIN      5 // Arduino pin tied to echo pin on ping sensor.
#define MAX_DISTANCE 500 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar (TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
unsigned int pingSpeed = 50; // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;


void setup() {
  Serial.begin(115200);
  printf_begin();
  
  Serial.print(F("\n\rCURB ALERT -- SENSOR UNIT"));

  radio.begin();
  radio.setAutoAck(1);                    // Ensure autoACK is enabled
  radio.enableAckPayload();               // Allow optional ack payloads
  radio.setRetries(0,15);                 // Smallest time between retries, max no. of retries
  radio.setPayloadSize(1);                // Here we are sending 1-byte payloads to test the call-response speed
  radio.openWritingPipe(pipes[1]);        // Both radios listen on the same pipes by default, and switch when writing
  radio.openReadingPipe(1,pipes[0]);
  radio.startListening();                 // Start listening
  radio.printDetails();                   // Dump the configuration of the rf unit for debugging
}

void loop() {
  

}
