/*******************************************************************************
 * CURB ALERT -- HEAD UNIT
 * 
 * This sketch will ping each remote sensor and get back the sensor data from
 * each unit.
 *  
********************************************************************************/

 /** RADIO PINOUTS *******

    1 GND   --> GND
    2 VCC   --> 3.3/5V
    3 CE    --> D9
    4 CSN   --> D10
    5 SCK   --> D13
    6 MOSI  --> D11
    7 MISO  --> D12
    8 IRQ   --> NOT USED
 
 ************************/

/* nRF24L01 libs */
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/* radio setup */
RF24 radio(9, 10);
const uint64_t pipes[3] = { 0xE8E8F0F0E1LL, 0xE8E8F0F0E2LL, 0xE8E8F0F0E3LL }; // head unit, front sensor, rear sensor


void setup() {
  Serial.begin(115200);
  printf_begin();
  
  Serial.print(F("\n\rCURB ALERT -- HEAD UNIT"));

  radio.begin();
  radio.setAutoAck(1);                    // Ensure autoACK is enabled
  radio.enableAckPayload();               // Allow optional ack payloads
  radio.setRetries(0,15);                 // Smallest time between retries, max no. of retries
  radio.setPayloadSize(1);                // Here we are sending 1-byte payloads to test the call-response speed
  radio.openWritingPipe(pipes[1]);        // Both radios listen on the same pipes by default, and switch when writing
  radio.openReadingPipe(1,pipes[0]);
  radio.startListening();                 // Start listening
  radio.printDetails();                   // Dump the configuration of the rf unit for debugging
}

void loop() {

}
